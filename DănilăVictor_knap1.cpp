//KnapSack Problem

#include<fstream>
using namespace std;

float M[100][3];
int W; /*(Weight)*/
int n;
float I; /*(Income)*/

int readData(){
    ifstream f("date.in");
    f>>W>>n;
    for(int i=1;i<=n;i++){
        f>>M[i][1]>>M[i][2];}}

int fillData(){
    for(int i=1;i<=n;i++){
        M[i][3]=M[i][2]/M[i][1];}}


int sortData(){
    int i=1;
    int place;
    while(i<=n){
        float minim=INT_MAX;
        for(int j=i;j<=n;j++){
            if(M[j][3]<minim){
                minim=M[j][3];
                place=j;}}
        for(int k=1;k<=3;k++){
            swap(M[i][k],M[place][k]);}
        i++;}}

int printData(){
    ofstream g("date.out");
    for(int i=1;i<=n;i++){
        for(int j=1;j<=3;j++){
            g<<M[i][j]<<" ";}
        g<<endl;}
    g<<I;}

float greedy(){
    int WAll=0;
    for(int i=n;i>=1;i++){
        if(WAll+M[i][1]<W){
            WAll=WAll+M[i][1];
            I=I+M[i][3];}
        else if(WAll+M[i][1]==W){
            I=I+M[i][2];
            break;}
        else{I=I+M[i][3]*(W-WAll);}}}


int main(){
    readData();
    fillData();
    sortData();
    greedy();
    printData();
    return 1;}
